// function imprimirUso () {
//     console.info(`node ${process.argv[1]} <nombre de archivo json> <llave> <llaveNueva> [archivo out]`);
// }

const CONFIG = require('./cambiarLlavesConfig.js');

const { nombreDeArchivo, llave, llaveNueva, nombreDeOut } = CONFIG;

// const [nombreDeArchivo, llave, llaveNueva, nombreDeOut] = [
//     process.argv[2],
//     process.argv[3],
//     process.argv[4],
//     process.argv[5],
// ];

// if (!nombreDeArchivo || !llave || !llaveNueva) {
//     console.error('por favor poner los argumentos requeridos');
//     imprimirUso();
//     process.exit(1);
// }

const fs = require('fs');

const archivo = fs.readFileSync(nombreDeArchivo);

let json = JSON.parse(archivo);

json = json.map(
    el => {
        if (llaveNueva === 'DELETT') {
            delete el[llave];
            return el;
        }
        el[llaveNueva] = el[llave];
        delete el[llave];
        return el;
    },
);

console.log(JSON.stringify(json));

if (nombreDeOut) fs.writeFileSync(nombreDeOut, JSON.stringify(json));